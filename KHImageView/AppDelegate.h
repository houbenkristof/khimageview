//
//  AppDelegate.h
//  KHImageView
//
//  Created by Kristof Houben on 10/02/14.
//  Copyright (c) 2014 Kristof Houben. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
