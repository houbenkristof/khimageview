//
//  MainViewController.m
//  KHImageView
//
//  Created by Kristof Houben on 10/02/14.
//  Copyright (c) 2014 Kristof Houben. All rights reserved.
//

#import "MainViewController.h"
#import "KHImageView.h"

@interface MainViewController ()

@end

@implementation MainViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)showImage:(id)sender {
    KHImageView *imageView = [[KHImageView alloc] initWithImage:[UIImage imageNamed:@"hipster.jpg"]];
    [imageView show];
}

@end
