//
//  KHImageView.h
//  KHImageView
//
//  Created by Kristof Houben on 10/02/14.
//  Copyright (c) 2014 Kristof Houben. All rights reserved.
//

#import <UIKit/UIKit.h>

@class KHImageView;

typedef void(^KHImageViewHandler)(KHImageView *imageView);

@interface KHImageView : UIView

-(instancetype)initWithImage:(UIImage *)image;

-(void)show;

@end
