//
//  KHImageView.m
//  KHImageView
//
//  Created by Kristof Houben on 10/02/14.
//  Copyright (c) 2014 Kristof Houben. All rights reserved.
//

#import "KHImageView.h"

@interface KHImageView ()

@property (strong, nonatomic) UIImage *image;
@property (weak, nonatomic) IBOutlet UIView *backgroundView;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@property (nonatomic, copy) KHImageViewHandler handler;
@property (nonatomic, strong) KHImageView *retainedSelf;

@property (nonatomic, strong) UIDynamicAnimator *animator;

@end

@implementation KHImageView

- (instancetype)initWithImage:(UIImage *)image
{
    if (!(self = [super init])) return nil;
    
    // set up our UIKit Dynamics
    
    self.animator = [[UIDynamicAnimator alloc] initWithReferenceView:self];
    
    
    // set image
    
    self.image = image;
    
    [self setup];
    
    return self;
}

- (void)setup
{
    
    UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
    
    [self addSubview:[[[NSBundle mainBundle] loadNibNamed:@"KHImageView" owner:self options:nil] firstObject]];
    
    // set bounds
    self.frame = keyWindow.bounds;
    
    // setup background layer
    self.backgroundView.alpha = 0;
    self.backgroundView.frame = self.frame;
    
    // setup image view
    self.imageView.image = self.image;
    self.imageView.frame = CGRectMake(20, -280, 280, 280);
    self.imageView.layer.cornerRadius = 4;
    self.imageView.layer.masksToBounds = YES;
}

- (void)show
{
    UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
    [keyWindow addSubview:self];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
    
    [UIView animateWithDuration:.3 animations:^{
        self.backgroundView.alpha = 1;
        keyWindow.tintAdjustmentMode = UIViewTintAdjustmentModeAutomatic;
        [keyWindow tintColorDidChange];
    }];
    
    // snap the image to center
    
    UISnapBehavior *snapBehaviour = [[UISnapBehavior alloc] initWithItem:self.imageView snapToPoint:keyWindow.center];
    snapBehaviour.damping = 0.3f;
    [self.animator addBehavior:snapBehaviour];
    
    // panning gesture

    UIGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];
    [self.imageView addGestureRecognizer:pan];
}

#pragma mark - Actions

- (void)handlePan:(UIPanGestureRecognizer *)gesture
{
    UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
    
    static UIAttachmentBehavior *attachment;
    static CGPoint               startCenter;
    
    // variables for calculating angular velocity
    
    static CFAbsoluteTime        lastTime;
    static CGFloat               lastAngle;
    static CGFloat               angularVelocity;
    
    if (gesture.state == UIGestureRecognizerStateBegan)
    {
        [self.animator removeAllBehaviors];
        
        startCenter = gesture.view.center;
        
        // calculate the center offset and anchor point
        
        CGPoint pointWithinAnimatedView = [gesture locationInView:gesture.view];
        
        UIOffset offset = UIOffsetMake(pointWithinAnimatedView.x - gesture.view.bounds.size.width / 2.0,
                                       pointWithinAnimatedView.y - gesture.view.bounds.size.height / 2.0);
        
        CGPoint anchor = [gesture locationInView:gesture.view.superview];
        
        // create attachment behavior
        
        attachment = [[UIAttachmentBehavior alloc] initWithItem:gesture.view
                                               offsetFromCenter:offset
                                               attachedToAnchor:anchor];
        
        // code to calculate angular velocity (seems curious that I have to calculate this myself, but I can if I have to)
        
        lastTime = CFAbsoluteTimeGetCurrent();
        lastAngle = [self angleOfView:gesture.view];
        
        attachment.action = ^{
            CFAbsoluteTime time = CFAbsoluteTimeGetCurrent();
            CGFloat angle = [self angleOfView:gesture.view];
            if (time > lastTime) {
                angularVelocity = (angle - lastAngle) / (time - lastTime);
                lastTime = time;
                lastAngle = angle;
            }
        };
        
        // add attachment behavior
        
        [self.animator addBehavior:attachment];
    }
    else if (gesture.state == UIGestureRecognizerStateChanged)
    {
        // as user makes gesture, update attachment behavior's anchor point, achieving drag 'n' rotate
        
        CGPoint anchor = [gesture locationInView:gesture.view.superview];
        attachment.anchorPoint = anchor;
    }
    else if (gesture.state == UIGestureRecognizerStateEnded)
    {
        [self.animator removeAllBehaviors];
        
        CGPoint velocity = [gesture velocityInView:gesture.view.superview];
        
        // if we aren't dragging it down or dragging it fast enough, just snap it back and quit
        
        if ((fabs(atan2(velocity.y, velocity.x) - M_PI_2) > M_PI_4) || velocity.y < 1200) {
            UISnapBehavior *snap = [[UISnapBehavior alloc] initWithItem:gesture.view snapToPoint:keyWindow.center];
            [self.animator addBehavior:snap];
            
            return;
        }
        
        // otherwise, create UIDynamicItemBehavior that carries on animation from where the gesture left off (notably linear and angular velocity)
        
        UIDynamicItemBehavior *dynamic = [[UIDynamicItemBehavior alloc] initWithItems:@[gesture.view]];
        [dynamic addLinearVelocity:velocity forItem:gesture.view];
        [dynamic addAngularVelocity:angularVelocity forItem:gesture.view];
        [dynamic setAngularResistance:2];
        
        // when the view no longer intersects with its superview, go ahead and remove it
        
        __block BOOL isAnimated = FALSE;
        
        dynamic.action = ^{
            if (!CGRectIntersectsRect(gesture.view.superview.bounds, gesture.view.frame)) {
                
                if (!isAnimated) {
                    isAnimated = YES;
                    
                    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:YES];
                    
                    [UIView animateWithDuration:.3 animations:^{
                        self.backgroundView.alpha = 0;
                        keyWindow.tintAdjustmentMode = UIViewTintAdjustmentModeAutomatic;
                        [keyWindow tintColorDidChange];
                        NSLog(@"animation");
                    } completion:^(BOOL finished){
                        if(finished){
                            [self.animator removeAllBehaviors];
                            [self removeFromSuperview];
                            self.retainedSelf = nil;
                            isAnimated = NO;
                            NSLog(@"completed");
                            
                            
                        }
                    }];
                }

            }
        };
        
        [self.animator addBehavior:dynamic];
    }
}

- (CGFloat)angleOfView:(UIView *)view
{
    // http://stackoverflow.com/a/2051861/1271826
    
    return atan2(view.transform.b, view.transform.a);
}

@end
