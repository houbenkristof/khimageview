//
//  MainViewController.h
//  KHImageView
//
//  Created by Kristof Houben on 10/02/14.
//  Copyright (c) 2014 Kristof Houben. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainViewController : UIViewController

- (IBAction)showImage:(id)sender;

@end
